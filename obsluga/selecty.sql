BEGIN;

/*
suma punktów lekarza za dany przedzial czasowy z wizyt refundowanych lub prywatnych
*/
CREATE OR REPLACE FUNCTION licz_punkty_lekarza(
		szukany_lekarz_id integer, 
		data_poczatek date, 
		data_koniec date,
		wizyty_prywatne boolean
	) 
	RETURNS TABLE (
		sum bigint
	) AS 
	$$

	DECLARE
	szukany_lekarz_id ALIAS FOR $1;
	data_poczatek ALIAS FOR $2;
	data_koniec ALIAS FOR $3;
	wizyty_prywatne ALIAS FOR $4;

	BEGIN
		RETURN QUERY
		SELECT sum(projekt.punkty_i_wyceny.liczba_punktow)
		FROM projekt.punkty_i_wyceny 
		JOIN projekt.wizyta 
		ON projekt.punkty_i_wyceny.rodzaj_wizyty=projekt.wizyta.rodzaj_wizyty
		WHERE projekt.wizyta.lekarz_id=szukany_lekarz_id
		AND projekt.wizyta.data_wizyty>=data_poczatek
		AND projekt.wizyta.data_wizyty<=data_koniec
		AND projekt.wizyta.wizyta_prywatna=wizyty_prywatne;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca stawkę miesięczną danego pracownika
*/
CREATE OR REPLACE FUNCTION stawka_pracownika(szukany_pracownik_id integer) 
	RETURNS TABLE (
		stawka_refundowane int,
		stawka_komercyjne_dla_lekarzy int,
		rodzaj_stawki text
	) AS 
	$$

	DECLARE
	szukany_pracownik_id ALIAS FOR $1;

	BEGIN
		RETURN QUERY

		SELECT 
			projekt.stawki_pracownicze.stawka_refundowane,
			projekt.stawki_pracownicze.stawka_komercyjne,
			CASE 
				WHEN projekt.pracownik.nazwa_stanowiska='lekarz'
				THEN '%'
				ELSE '[PLN]'
			END
		FROM projekt.stawki_pracownicze 
		JOIN projekt.pracownik 
		ON projekt.pracownik.nazwa_stanowiska=projekt.stawki_pracownicze.nazwa_stanowiska 
		WHERE projekt.pracownik.pracownik_id=szukany_pracownik_id;
	END;
	$$ LANGUAGE plpgsql;


/*
oblicza wyplatę dla lekarza za dany okres rozliczeniowy wliczajac wizyty refundowane i prywatne
*/
CREATE OR REPLACE FUNCTION wyplata_lekarza_za_okres(
		szukany_lekarz_id integer, 
		data_poczatek date, 
		data_koniec date,
		OUT wyplata real
	) AS 
	$$

	DECLARE
	szukany_lekarz_id ALIAS FOR $1;
	data_poczatek ALIAS FOR $2;
	data_koniec ALIAS FOR $3;
	stawka_za_punkt real;
	temp_wyplata_refundowane real;
	temp_wyplata_prywatne real;
	stawka_lekarza_refundowane real;
	stawka_lekarza_komercyjne real;

	BEGIN
		stawka_za_punkt = (
			SELECT projekt.oddzial.stawka_per_punkt_nfz 
			FROM projekt.oddzial
			JOIN projekt.pracownik 
			ON projekt.pracownik.oddzial_id=projekt.oddzial.oddzial_id 
			WHERE projekt.pracownik.pracownik_id=szukany_lekarz_id
		);
		stawka_lekarza_refundowane = (
			SELECT stawka_refundowane 
			FROM stawka_pracownika(szukany_lekarz_id)
		);
		stawka_lekarza_komercyjne = (
			SELECT stawka_komercyjne_dla_lekarzy 
			FROM stawka_pracownika(szukany_lekarz_id)
		);
		temp_wyplata_refundowane = licz_punkty_lekarza(szukany_lekarz_id, data_poczatek, data_koniec, FALSE) * stawka_za_punkt * stawka_lekarza_refundowane / 100.0;
		temp_wyplata_prywatne = licz_punkty_lekarza(szukany_lekarz_id, data_poczatek, data_koniec, TRUE) * stawka_lekarza_komercyjne / 100.0;
		
		IF temp_wyplata_refundowane > 0.0
		THEN
			wyplata = temp_wyplata_refundowane;
		ELSE
			wyplata = 0.0;
		END IF;
		IF temp_wyplata_prywatne > 0.0
		THEN
			wyplata = wyplata + temp_wyplata_prywatne;
		END IF;

	END;
	$$ LANGUAGE plpgsql;


/*
zwraca tabelę wizyt ograniczoną danym limitem na danej karcie
*/
CREATE OR REPLACE FUNCTION wizyty_limit(wielkosc_limitu integer, karta integer) 
	RETURNS TABLE (
		wizyta_id integer,
		oddzial_id varchar,
		rodzaj_wizyty varchar,
		pesel_pacjenta varchar(11),
		raport varchar,
		wizyta_prywatna boolean,
		lekarz_id integer,
		data_wizyty date
	) AS 
	$$

	DECLARE
	wielkosc_limitu ALIAS FOR $1;
	karta ALIAS FOR $2;

	BEGIN
		RETURN QUERY

		SELECT * 
		FROM projekt.wizyta 
		WHERE projekt.wizyta.wizyta_id>(karta-1)*wielkosc_limitu
		AND projekt.wizyta.wizyta_id<=karta*wielkosc_limitu;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca tabelę informacji o pacjentach z ograniczonym danym limitem na danej karcie
*/
CREATE OR REPLACE FUNCTION pacjenci_limit(wielkosc_limitu integer, karta integer) 
	RETURNS TABLE (
		nazwisko VARCHAR,
		imie VARCHAR,
		pesel_pacjenta VARCHAR(11),
        telefon INTEGER,
        data_urodzenia DATE,
        miejsce_urodzenia VARCHAR,
        ubezpieczenie_emeryt_rencista BOOLEAN,
        ubezpieczenie_zatrudniony BOOLEAN,
        ubezpieczenie_informacja VARCHAR
	) AS 
	$$

	DECLARE
	wielkosc_limitu ALIAS FOR $1;
	karta ALIAS FOR $2;

	BEGIN
		RETURN QUERY
		SELECT
			pacjenci_info.nazwisko,
			pacjenci_info.imie,
			pacjenci_info.pesel_pacjenta,
			pacjenci_info.telefon,
			pacjenci_info.data_urodzenia,
			pacjenci_info.miejsce_urodzenia,
			pacjenci_info.ubezpieczenie_emeryt_rencista,
			pacjenci_info.ubezpieczenie_zatrudniony,
			pacjenci_info.ubezpieczenie_informacja
		FROM
		(SELECT 
			row_number()
				OVER
				(ORDER BY projekt.pacjent.nazwisko, projekt.pacjent.imie, projekt.pacjent.pesel_pacjenta nulls last) AS numer_porzadkowy,
			projekt.pacjent.nazwisko, 
			projekt.pacjent.imie, 
			projekt.pacjent.pesel_pacjenta, 
			projekt.pacjent.telefon, 
			projekt.pacjent.data_urodzenia, 
			projekt.pacjent.miejsce_urodzenia, 
			projekt.pacjent.ubezpieczenie_emeryt_rencista, 
			projekt.pacjent.ubezpieczenie_zatrudniony, 
			projekt.pacjent.ubezpieczenie_informacja 
		FROM projekt.pacjent
		) AS pacjenci_info
		WHERE pacjenci_info.numer_porzadkowy>(karta-1)*wielkosc_limitu
		AND pacjenci_info.numer_porzadkowy<=karta*wielkosc_limitu;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca tabelę informacji o adresie pacjenta o danym peselu
*/
CREATE OR REPLACE FUNCTION adres_pacjenta(dany_pesel VARCHAR(11)) 
	RETURNS TABLE (
		nazwisko VARCHAR,
		imie VARCHAR,
		pesel_pacjenta VARCHAR(11),
		ulica VARCHAR,
		numer_domu VARCHAR,
		kod_pocztowy VARCHAR(6),
		miejscowosc VARCHAR
	) AS 
	$$

	BEGIN
		RETURN QUERY

		SELECT
			projekt.pacjent.nazwisko,
			projekt.pacjent.imie,
			projekt.pacjent.pesel_pacjenta,
			projekt.adres_zamieszkania.ulica,
			projekt.adres_zamieszkania.numer_domu,
			projekt.adres_zamieszkania.kod_pocztowy,
			projekt.adres_zamieszkania.miejscowosc
		FROM
			projekt.pacjent
		JOIN
			projekt.adres_zamieszkania
		ON
			projekt.pacjent.pesel_pacjenta=adres_zamieszkania.pesel_pacjenta
		WHERE projekt.adres_zamieszkania.pesel_pacjenta=dany_pesel;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca wyplate pracownika (nie lekarza) za dany okres (miesiac)
*/
CREATE OR REPLACE FUNCTION wyplata_pracownika_za_okres(
		szukany_pracownik_id integer, 
		OUT wyplata real
	) AS 
	$$

	DECLARE
	szukany_pracownik_id ALIAS FOR $1;
	stawka_miesieczna real;

	BEGIN
		stawka_miesieczna = (
			SELECT projekt.stawki_pracownicze.stawka_refundowane 
			FROM projekt.stawki_pracownicze
			JOIN projekt.pracownik 
			ON projekt.pracownik.nazwa_stanowiska=projekt.stawki_pracownicze.nazwa_stanowiska
			WHERE projekt.pracownik.pracownik_id=szukany_pracownik_id
		);

		wyplata = stawka_miesieczna;
		
		IF stawka_miesieczna > 0.0
		THEN
			wyplata = stawka_miesieczna;
		ELSE
			wyplata = 0.0;
		END IF;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca lokalizacje oddzialu o danym id (najczęściej miejscowości)
*/
CREATE OR REPLACE FUNCTION lokalizacja_oddzialu(id_oddzialu VARCHAR) 
	RETURNS TABLE (
        ulica VARCHAR,
        numer_lokalu VARCHAR,
        kod_pocztowy VARCHAR(6),
        miejscowosc VARCHAR   
	) AS 
	$$

	BEGIN
		RETURN QUERY

		SELECT 
			projekt.lokalizacja_oddzialu.ulica,
			projekt.lokalizacja_oddzialu.numer_lokalu,
			projekt.lokalizacja_oddzialu.kod_pocztowy,
			projekt.lokalizacja_oddzialu.miejscowosc
		FROM
			projekt.lokalizacja_oddzialu
		WHERE projekt.lokalizacja_oddzialu.oddzial_id=id_oddzialu;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca informacje na temat osoby (osob) upowaznionych dla pacjenta
*/
CREATE OR REPLACE FUNCTION osoba_upowazniona_pacjenta(id_pacjenta VARCHAR(11)) 
	RETURNS TABLE (
        nazwisko VARCHAR,
        imie VARCHAR,
        telefon INTEGER
	) AS 
	$$

	BEGIN
		RETURN QUERY

		SELECT 
			projekt.osoba_upowazniona.nazwisko,
			projekt.osoba_upowazniona.imie,
			projekt.osoba_upowazniona.telefon
		FROM
			projekt.osoba_upowazniona
		WHERE projekt.osoba_upowazniona.pesel_pacjenta=id_pacjenta;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca informacje na temat sprzetów medycznych w oddziale i informacji z paszportu
*/
CREATE OR REPLACE FUNCTION sprzet_oddzialu(id_oddzialu VARCHAR) 
	RETURNS TABLE (
        nazwa VARCHAR,
        rok_produkcji INTEGER,
        numer_seryjny_sprzetu VARCHAR,
        liczba_przegadow BIGINT,
        data_ostatniego_przegladu DATE,
        data_waznosci_paszportu DATE,
        dni_do_konca_waznosci INTEGER,
        uwagi VARCHAR,
        komunikaty TEXT
	) AS 
	$$

	BEGIN
		RETURN QUERY

		SELECT 
			projekt.sprzet_medyczny.nazwa,
			projekt.sprzet_medyczny.rok_produkcji,
			aktualny_paszport_techniczny.numer_seryjny,
			aktualny_paszport_techniczny.liczba_przegadow,
			aktualny_paszport_techniczny.data_ostatniego_przegladu,
			aktualny_paszport_techniczny.data_waznosci,
			aktualny_paszport_techniczny.dni_do_konca_waznosci,
			aktualny_paszport_techniczny.uwagi,
			CASE 
				WHEN aktualny_paszport_techniczny.dni_do_konca_waznosci < 0
				THEN 'NIEAKTUALNY PASZPORT'
				ELSE ''
			END AS komunikaty
		FROM
			(
				SELECT /* paszport techniczny z aktualnymi informacjami */
					obecna_data_waznosci.numer_seryjny,
					obecna_data_waznosci.liczba_przegadow,
					obecna_data_waznosci.data_waznosci,
					projekt.paszport_techniczny.data_przegladu as data_ostatniego_przegladu,
					projekt.paszport_techniczny.uwagi,
					obecna_data_waznosci.data_waznosci - CURRENT_DATE as dni_do_konca_waznosci
				FROM (
					SELECT 
						COUNT(przeglad_id) as liczba_przegadow,
						numer_seryjny, 
						MAX(data_waznosci) as data_waznosci
					FROM projekt.paszport_techniczny 
					GROUP BY numer_seryjny
				) AS obecna_data_waznosci
				JOIN
					projekt.paszport_techniczny
				ON
					projekt.paszport_techniczny.data_waznosci=obecna_data_waznosci.data_waznosci
				AND
					projekt.paszport_techniczny.numer_seryjny=obecna_data_waznosci.numer_seryjny
			) AS aktualny_paszport_techniczny
		JOIN
			projekt.sprzet_medyczny
		ON
			projekt.sprzet_medyczny.numer_seryjny=aktualny_paszport_techniczny.numer_seryjny
		WHERE projekt.sprzet_medyczny.oddzial_id=id_oddzialu;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca informacje na temat limitow punktowych dla oddzialu
*/
CREATE OR REPLACE FUNCTION limity_oddzialu(
		id_oddzialu VARCHAR
	) 
	RETURNS TABLE (
		data_poczatek_rozliczenia DATE,
		oddzial_id VARCHAR,
		liczba_punktow INTEGER,
		data_koniec_rozliczenia DATE
	) AS 
	$$

	BEGIN
		RETURN QUERY

		SELECT *
		FROM projekt.limit_punktowy
		WHERE projekt.limit_punktowy.oddzial_id=id_oddzialu;
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca informacje na temat punktow zebranych w oddziale za dany okres (występujący w tablicy limitów)
*/
CREATE OR REPLACE FUNCTION punkty_oddzialu_za_okres(
		id_oddzialu VARCHAR, 
		data_poczatek date, 
		data_koniec date
	) 
	RETURNS TABLE (
		data_poczatek_rozliczenia DATE,
		data_koniec_rozliczenia DATE,
		zalozona_liczba_punktow INTEGER,
		rzeczywista_liczba_punktow BIGINT
	) AS 
	$$

	DECLARE
	exist_status_data_poczatek BOOLEAN;
	exist_status_data_koniec BOOLEAN;

	BEGIN
		/*
		sprawdzanie poprawnosci danych wejsciowych
		*/
		exist_status_data_poczatek = (
			SELECT NOT EXISTS (
				SELECT limity.data_poczatek_rozliczenia 
				FROM limity_oddzialu(id_oddzialu) AS limity
				WHERE limity.data_poczatek_rozliczenia=data_poczatek LIMIT 1
			)
		);

		exist_status_data_koniec = (
			SELECT NOT EXISTS (
				SELECT limity.data_koniec_rozliczenia 
				FROM limity_oddzialu(id_oddzialu) AS limity
				WHERE limity.data_koniec_rozliczenia=data_koniec LIMIT 1
			)
		);

		IF exist_status_data_poczatek
		THEN
			RAISE EXCEPTION '% - nie ma takiej daty początkowej rozliczenia dla tego oddzialu', data_poczatek;
		END IF;

		IF exist_status_data_koniec
		THEN
			RAISE EXCEPTION '% - nie ma takiej daty końcowej rozliczenia dla tego oddzialu', data_koniec;
		END IF;

		RETURN QUERY
		SELECT 
			limity_oddzialu.data_poczatek_rozliczenia,
			limity_oddzialu.data_koniec_rozliczenia,
			limity_oddzialu.liczba_punktow AS zalozona_liczba_punktow,
			wizyty_i_punkty.liczba_punktow AS rzeczywista_liczba_punktow
		FROM limity_oddzialu(id_oddzialu) AS limity_oddzialu
		JOIN (
			SELECT 
				projekt.wizyta.oddzial_id,
				sum(projekt.punkty_i_wyceny.liczba_punktow) AS liczba_punktow
			FROM projekt.wizyta
			JOIN projekt.punkty_i_wyceny
			ON projekt.wizyta.rodzaj_wizyty=projekt.punkty_i_wyceny.rodzaj_wizyty
			WHERE projekt.wizyta.oddzial_id=id_oddzialu
			AND projekt.wizyta.wizyta_prywatna=FALSE
			AND projekt.wizyta.data_wizyty > data_poczatek
			AND projekt.wizyta.data_wizyty <= data_koniec
			GROUP BY projekt.wizyta.oddzial_id
		) AS wizyty_i_punkty
		ON limity_oddzialu.oddzial_id=wizyty_i_punkty.oddzial_id;
		
	END;
	$$ LANGUAGE plpgsql;


/*
zwraca tabelę pracowników danego oddzialu (lekarzy lub nie-lekarzy)
*/
CREATE OR REPLACE FUNCTION pracownicy_oddzialu(id_oddzialu varchar, czy_lekarz boolean) 
	RETURNS TABLE (
		pracownik_id INTEGER,
		nazwa_stanowiska varchar,
		data_podpisania_umowy date,
		data_rozwiazania_umowy date,
		imie varchar,
		nazwisko varchar
	) AS 
	$$

	BEGIN
		IF czy_lekarz
		THEN
			RETURN QUERY
			SELECT
				projekt.pracownik.pracownik_id,
				projekt.pracownik.nazwa_stanowiska,
				projekt.pracownik.data_podpisania_umowy,
				projekt.pracownik.data_rozwiazania_umowy,
				projekt.pracownik.imie,
				projekt.pracownik.nazwisko
			FROM projekt.pracownik 
			WHERE projekt.pracownik.nazwa_stanowiska='lekarz' 
			AND oddzial_id=id_oddzialu;
		ELSE
			RETURN QUERY
			SELECT
				projekt.pracownik.pracownik_id,
				projekt.pracownik.nazwa_stanowiska,
				projekt.pracownik.data_podpisania_umowy,
				projekt.pracownik.data_rozwiazania_umowy,
				projekt.pracownik.imie,
				projekt.pracownik.nazwisko
			FROM projekt.pracownik 
			WHERE projekt.pracownik.nazwa_stanowiska<>'lekarz' 
			AND oddzial_id=id_oddzialu;
		END IF;
	END;
	$$ LANGUAGE plpgsql;



/*
zwraca informacje na temat zarobku w oddziale za dany okres (występujący w tablicy limitów)
*/
CREATE OR REPLACE FUNCTION zarobek_oddzialu_za_okres(
		id_oddzialu VARCHAR, 
		data_poczatek date, 
		data_koniec date,
		OUT stan_pieniezny real
	) AS 
	$$

	DECLARE
	exist_status_data_poczatek BOOLEAN;
	exist_status_data_koniec BOOLEAN;
	pieniadze_z_wizyt_prywatnych real;
	temp_pieniadze_z_wizyt_prywatnych real;
	punkty_z_wizyt_refundowanych INTEGER;
	temp_punkty_z_wizyt_refundowanych INTEGER;
	pieniadze_z_wizyt_refundowanych real;
	temp_pieniadze_z_wizyt_refundowanych real;
	przelicznik_oddzialu_jednego_punktu real;
	stawka_za_punkt real;
	pieniadze_dla_lekarzy real;
	pieniadze_dla_pracownikow real;
	ilosc_miesiecy_do_wyplaty INTEGER;
	stawka_pracownika_z_id real;
	row RECORD;

	BEGIN
		/*
		sprawdzanie poprawnosci danych wejsciowych
		*/
		exist_status_data_poczatek = (
			SELECT NOT EXISTS (
				SELECT limity.data_poczatek_rozliczenia 
				FROM limity_oddzialu(id_oddzialu) AS limity
				WHERE limity.data_poczatek_rozliczenia=data_poczatek LIMIT 1
			)
		);

		exist_status_data_koniec = (
			SELECT NOT EXISTS (
				SELECT limity.data_koniec_rozliczenia 
				FROM limity_oddzialu(id_oddzialu) AS limity
				WHERE limity.data_koniec_rozliczenia=data_koniec LIMIT 1
			)
		);

		IF exist_status_data_poczatek
		THEN
			RAISE EXCEPTION '% - nie ma takiej daty początkowej rozliczenia dla tego oddzialu', data_poczatek;
		END IF;

		IF exist_status_data_koniec
		THEN
			RAISE EXCEPTION '% - nie ma takiej daty końcowej rozliczenia dla tego oddzialu', data_koniec;
		END IF;

		temp_pieniadze_z_wizyt_prywatnych = (
			SELECT 
				sum(projekt.punkty_i_wyceny.oplata_jesli_prywatna) AS przychody
			FROM projekt.wizyta
			JOIN projekt.punkty_i_wyceny
			ON projekt.wizyta.rodzaj_wizyty=projekt.punkty_i_wyceny.rodzaj_wizyty
			WHERE projekt.wizyta.oddzial_id=id_oddzialu
			AND projekt.wizyta.wizyta_prywatna=TRUE
			AND projekt.wizyta.data_wizyty > data_poczatek
			AND projekt.wizyta.data_wizyty <= data_koniec
			GROUP BY projekt.wizyta.oddzial_id
		);

		IF temp_pieniadze_z_wizyt_prywatnych > 0.0
		THEN
			pieniadze_z_wizyt_prywatnych = temp_pieniadze_z_wizyt_prywatnych;
		ELSE
			pieniadze_z_wizyt_prywatnych = 0.0;
		END IF;

		temp_punkty_z_wizyt_refundowanych = (
			SELECT
				sum(projekt.punkty_i_wyceny.liczba_punktow) AS liczba_punktow
			FROM projekt.wizyta
			JOIN projekt.punkty_i_wyceny
			ON projekt.wizyta.rodzaj_wizyty=projekt.punkty_i_wyceny.rodzaj_wizyty
			WHERE projekt.wizyta.oddzial_id=id_oddzialu
			AND projekt.wizyta.wizyta_prywatna=FALSE
			AND projekt.wizyta.data_wizyty > data_poczatek
			AND projekt.wizyta.data_wizyty <= data_koniec
			GROUP BY projekt.wizyta.oddzial_id
		);

		IF temp_punkty_z_wizyt_refundowanych > 0.0
		THEN
			punkty_z_wizyt_refundowanych = temp_punkty_z_wizyt_refundowanych;
		ELSE
			punkty_z_wizyt_refundowanych = 0.0;
		END IF;

		stawka_za_punkt = (
			SELECT projekt.oddzial.stawka_per_punkt_nfz 
			FROM projekt.oddzial
			WHERE projekt.oddzial.oddzial_id=id_oddzialu
		);

		pieniadze_z_wizyt_refundowanych = stawka_za_punkt * punkty_z_wizyt_refundowanych;

		/*
		liczenie wypłat lekarzy
		*/
		pieniadze_dla_lekarzy = 0.0;
		FOR row IN SELECT * FROM pracownicy_oddzialu(id_oddzialu, TRUE)
		LOOP
			pieniadze_dla_lekarzy = pieniadze_dla_lekarzy + wyplata_lekarza_za_okres(
				row.pracownik_id, 
				data_poczatek, 
				data_koniec);
		END LOOP;

		/*
		liczenie wypłat normalnych pracowników
		*/
		pieniadze_dla_pracownikow = 0.0;
		ilosc_miesiecy_do_wyplaty = (
			SELECT EXTRACT(YEAR FROM age) * 12 + EXTRACT(MONTH FROM age) AS months_between
			FROM age(data_koniec, data_poczatek) AS t(age)
		);
		FOR row IN SELECT * FROM pracownicy_oddzialu(id_oddzialu, FALSE)
		LOOP
			stawka_pracownika_z_id = (
				SELECT stawka_refundowane
				FROM stawka_pracownika(row.pracownik_id)
			);
			pieniadze_dla_pracownikow = ilosc_miesiecy_do_wyplaty * stawka_pracownika_z_id + pieniadze_dla_pracownikow;
		END LOOP;

		/*
		ostateczne liczenie stanu pieniężnego
		*/
		stan_pieniezny = pieniadze_z_wizyt_prywatnych + pieniadze_z_wizyt_refundowanych - pieniadze_dla_lekarzy - pieniadze_dla_pracownikow;
		
	END;
	$$ LANGUAGE plpgsql;

COMMIT;
