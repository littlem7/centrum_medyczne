/*
przykładowe inserty do tabeli
*/

BEGIN;
INSERT INTO projekt.firma VALUES('Laser');
INSERT INTO projekt.oddzial VALUES('Kraków', 'Laser', 8.70);
INSERT INTO projekt.oddzial VALUES('Warszawa', 'Laser', 9.50);
INSERT INTO projekt.oddzial VALUES('Chrzanów', 'Laser', 7.00);
INSERT INTO projekt.lokalizacja_oddzialu VALUES('Kraków', '11-234', 'Kraków', 'Widna', '3A');
INSERT INTO projekt.lokalizacja_oddzialu VALUES('Warszawa', '14-443', 'Warszawa', 'Ciemna', '2C');
INSERT INTO projekt.lokalizacja_oddzialu VALUES('Chrzanów', '32-500', 'Chrzanów', 'Szeroka', '36');
INSERT INTO projekt.sprzet_medyczny VALUES('AWF783', 'tomograf komputerowy', '2013', 'Kraków');
INSERT INTO projekt.sprzet_medyczny VALUES('S78', 'mikroskop operacyjny', '2011', 'Kraków');
INSERT INTO projekt.paszport_techniczny (numer_seryjny, data_przegladu, data_waznosci, uwagi) VALUES('AWF783', '2016-08-11', '2017-08-11', 'brak');
INSERT INTO projekt.paszport_techniczny (numer_seryjny, data_przegladu, data_waznosci, uwagi) VALUES('AWF783', '2015-09-07', '2017-09-07', 'brak');
INSERT INTO projekt.paszport_techniczny (numer_seryjny, data_przegladu, data_waznosci, uwagi) VALUES('S78', '2016-01-13', '2017-01-13', 'brak');
INSERT INTO projekt.limit_punktowy VALUES('2017-01-01', 'Kraków', 4500, '2017-02-01');
INSERT INTO projekt.limit_punktowy VALUES('2017-01-01', 'Warszawa', 4000, '2017-02-01');
INSERT INTO projekt.limit_punktowy VALUES('2017-01-01', 'Chrzanów', 4250, '2017-02-01');
INSERT INTO projekt.stawki_pracownicze VALUES('księgowy', 2500);
INSERT INTO projekt.stawki_pracownicze VALUES('lekarz', 40, 45);
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('księgowy', '2005-08-11', '2025-08-11', 'Anna', 'Abacka', 'Kraków');
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('księgowy', '2005-08-11', '2025-08-11', 'Błażej', 'Babacki', 'Warszawa');
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('księgowy', '2005-08-11', '2025-08-11', 'Cezary', 'Cabacki', 'Chrzanów');
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('lekarz', '2005-08-11', '2025-08-11', 'Daniel', 'Dadacki', 'Chrzanów');
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('lekarz', '2005-08-11', '2025-08-11', 'Ewelina', 'Ebacka', 'Kraków');
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('lekarz', '2005-08-11', '2025-08-11', 'Franciszek', 'Fabacki', 'Warszawa');
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('lekarz', '2005-08-11', '2025-08-11', 'Grzegorz', 'Gadacki', 'Chrzanów');
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('lekarz', '2005-08-11', '2025-08-11', 'Halina', 'Habacka', 'Kraków');
INSERT INTO projekt.pracownik (nazwa_stanowiska, data_podpisania_umowy, data_rozwiazania_umowy, imie, nazwisko, oddzial_id) VALUES('lekarz', '2005-08-11', '2025-08-11', 'Irena', 'Ibacka', 'Warszawa');
INSERT INTO projekt.pacjent VALUES('95030703789', 'Jolanta', 'Jabacka', 456293845, '1995-03-07', 'Chrzanów', FALSE, TRUE, '345-23-23-456');
INSERT INTO projekt.pacjent VALUES('94020703789', 'Kamil', 'Kabacki', 456293845, '1994-02-07', 'Kraków', FALSE, TRUE, '320-23-23-476');
INSERT INTO projekt.osoba_upowazniona VALUES(203475921, 'Lucyna', 'Lubacka', '95030703789');
INSERT INTO projekt.osoba_upowazniona VALUES(673849265, 'Łukasz', 'Łubacki', '95030703789');
INSERT INTO projekt.osoba_upowazniona VALUES(637482934, 'Mateusz', 'Mabacki', '94020703789');
INSERT INTO projekt.adres_zamieszkania VALUES('95030703789', '32-500', 'Chrzanów','Śląska', '38C');
INSERT INTO projekt.adres_zamieszkania VALUES('94020703789', '11-234', 'Kraków','Wąska', '1A');
INSERT INTO projekt.punkty_i_wyceny VALUES('W01', 5, 100.00);
INSERT INTO projekt.punkty_i_wyceny VALUES('W02', 4, 85.00);
INSERT INTO projekt.wizyta (oddzial_id, rodzaj_wizyty, pesel_pacjenta, raport, wizyta_prywatna, lekarz_id, data_wizyty) VALUES('Kraków', 'W01', '94020703789', 'Notatki z wizyty', FALSE, 5, '2017-01-13');
INSERT INTO projekt.wizyta (oddzial_id, rodzaj_wizyty, pesel_pacjenta, raport, wizyta_prywatna, lekarz_id, data_wizyty) VALUES('Kraków', 'W02', '94020703789', 'Notatki z wizyty', FALSE, 5, '2017-01-15');
INSERT INTO projekt.wizyta (oddzial_id, rodzaj_wizyty, pesel_pacjenta, raport, wizyta_prywatna, lekarz_id, data_wizyty) VALUES('Warszawa', 'W02', '94020703789', 'Notatki z wizyty', FALSE, 5, '2017-02-13');
INSERT INTO projekt.wizyta (oddzial_id, rodzaj_wizyty, pesel_pacjenta, raport, wizyta_prywatna, lekarz_id, data_wizyty) VALUES('Chrzanów', 'W01', '95030703789', 'Notatki z wizyty', TRUE, 5, '2017-02-14');
COMMIT;