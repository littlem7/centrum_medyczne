/*
usuwanie wszystkich tabel projektu stworzonych przez create_tables.sql i inserty.sql
usuwanie wszystkich widoków stworzonych przez widoki.sql
*/
DROP SCHEMA projekt cascade;


/*
usuwanie wszystkich funkcji do selekcji stworzonych przez selecty.sql
*/
DROP FUNCTION IF EXISTS licz_punkty_lekarza(integer, date, date, boolean);
DROP FUNCTION IF EXISTS stawka_pracownika(integer);
DROP FUNCTION IF EXISTS wyplata_lekarza_za_okres(integer, date, date);
DROP FUNCTION IF EXISTS wizyty_limit(integer, integer);
DROP FUNCTION IF EXISTS pacjenci_limit(integer, integer);
DROP FUNCTION IF EXISTS adres_pacjenta(varchar(11));
DROP FUNCTION IF EXISTS wyplata_pracownika_za_okres(integer);
DROP FUNCTION IF EXISTS lokalizacja_oddzialu(varchar);
DROP FUNCTION IF EXISTS osoba_upowazniona_pacjenta(varchar(11));
DROP FUNCTION IF EXISTS sprzet_oddzialu(varchar);
DROP FUNCTION IF EXISTS limity_oddzialu(varchar);
DROP FUNCTION IF EXISTS punkty_oddzialu_za_okres(varchar, date, date);
DROP FUNCTION IF EXISTS pracownicy_oddzialu(varchar, boolean);
DROP FUNCTION IF EXISTS zarobek_oddzialu_za_okres(varchar, date, date);


/*
usuwanie wszystkich funkcji do wstawiania pól
DROP FUNCTION IF EXISTS licz_punkty_lekarza(lekarz_id integer, data_poczatek date, data_koniec date);
*/
