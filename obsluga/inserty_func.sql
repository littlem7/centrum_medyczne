BEGIN TRANSACTION;

/*
funkcja dodaje oddział o danej nazwie i adresie
*/
CREATE OR REPLACE FUNCTION dodaj_oddzial(
		id_oddzialu varchar, 
		stawka_za_punkt real, 
		kod_pocztowy varchar(6), 
		miejscowosc varchar, 
		nazwa_ulicy varchar, 
		numer_budynku varchar,
		nazwa_firmy varchar
	) 
	RETURNS VOID AS 
	$$

	BEGIN
		INSERT INTO projekt.oddzial VALUES(
			id_oddzialu, 
			nazwa_firmy, 
			stawka_za_punkt
		);
		INSERT INTO projekt.lokalizacja_oddzialu VALUES(
			id_oddzialu, 
			kod_pocztowy, 
			miejscowosc, 
			nazwa_ulicy, 
			numer_budynku
		);
	END;
	$$ LANGUAGE plpgsql;


/*
funkcja dodaje sprzet medyczny do konkretnego oddzialu wraz z pierwszym przeglądem
*/
CREATE OR REPLACE FUNCTION dodaj_sprzet_medyczny(
		numer_seryjny varchar,
		nazwa varchar,
		rok_produkcji integer,
		oddzial_id varchar,
		data_przegladu date,
		data_waznosci date,
		uwagi varchar
	) 
	RETURNS VOID AS 
	$$

	BEGIN
		INSERT INTO projekt.sprzet_medyczny VALUES(
			numer_seryjny, 
			nazwa, 
			rok_produkcji, 
			oddzial_id
		);
		INSERT INTO projekt.paszport_techniczny (
			numer_seryjny, 
			data_przegladu, 
			data_waznosci, 
			uwagi
		) VALUES(
			numer_seryjny, 
			data_przegladu, 
			data_waznosci, 
			uwagi
		);
	END;
	$$ LANGUAGE plpgsql;


/*
funkcja dodaje limit punktowy dla danego oddzialu
*/
CREATE OR REPLACE FUNCTION dodaj_limit_punktowy_dla_oddzialu(
		data_poczatek_rozliczenia DATE,
        oddzial_id VARCHAR,
        liczba_punktow INTEGER,
        data_koniec_rozliczenia DATE
	) 
	RETURNS VOID AS 
	$$

	BEGIN
		INSERT INTO projekt.limit_punktowy VALUES(
			data_poczatek_rozliczenia, 
			oddzial_id, 
			liczba_punktow, 
			data_poczatek_rozliczenia
		);
	END;
	$$ LANGUAGE plpgsql;


/*
funkcja dodaje kolejną posadę wraz z zarobkami
*/
CREATE OR REPLACE FUNCTION dodaj_nowa_posade(
		nazwa_stanowiska varchar,
		stawka_pracownicza INTEGER
	) 
	RETURNS VOID AS 
	$$

	BEGIN
		INSERT INTO projekt.stawki_pracownicze VALUES(
			nazwa_stanowiska, 
			stawka_pracownicza
		);
	END;
	$$ LANGUAGE plpgsql;


/*
funkcja dodaje pracownika do konkretnego oddzialu
*/
CREATE OR REPLACE FUNCTION dodaj_pracownika(
		nazwa_stanowiska VARCHAR,
        data_podpisania_umowy DATE,
        data_rozwiazania_umowy DATE,
        imie VARCHAR,
        nazwisko VARCHAR,
        oddzial_id VARCHAR
	) 
	RETURNS VOID AS 
	$$

	BEGIN
		INSERT INTO projekt.pracownik (
			nazwa_stanowiska, 
			data_podpisania_umowy, 
			data_rozwiazania_umowy, 
			imie, 
			nazwisko, 
			oddzial_id
		) VALUES(
			nazwa_stanowiska,
			data_podpisania_umowy,
			data_rozwiazania_umowy,
			imie,
			nazwisko,
			oddzial_id
		);
	END;
	$$ LANGUAGE plpgsql;

/*
funkcja dodaje pacjenta do rejestru wraz z jego adresem i osoba upoważnioną
*/
CREATE OR REPLACE FUNCTION dodaj_pacjenta(
        pesel_pacjenta VARCHAR(11),
        imie VARCHAR,
        nazwisko VARCHAR,
        telefon INTEGER,
        data_urodzenia DATE,
        miejsce_urodzenia VARCHAR,
        ubezpieczenie_emeryt_rencista BOOLEAN,
        ubezpieczenie_zatrudniony BOOLEAN,
        ubezpieczenie_informacja VARCHAR,
        kod_pocztowy VARCHAR(6),
        miejscowosc_zamieszkania VARCHAR,
        ulica VARCHAR,
        numer_budynku VARCHAR,
        telefon_osoby_upowaznionej INTEGER,
        imie_osoby_upowaznionej VARCHAR,
        nazwisko_osoby_upowaznionej VARCHAR
	) 
	RETURNS VOID AS 
	$$

	BEGIN
		INSERT INTO projekt.pacjent VALUES(
			pesel_pacjenta,
			imie,
			nazwisko,
			telefon,
			data_urodzenia,
			miejsce_urodzenia,
			ubezpieczenie_emeryt_rencista,
			ubezpieczenie_zatrudniony,
			ubezpieczenie_informacja
		);
		INSERT INTO projekt.osoba_upowazniona VALUES(
			telefon_osoby_upowaznionej,
			imie_osoby_upowaznionej,
			nazwisko_osoby_upowaznionej,
			pesel_pacjenta
		);
		INSERT INTO projekt.adres_zamieszkania VALUES(
			pesel_pacjenta,
			kod_pocztowy,
			miejscowosc_zamieszkania,
			ulica,
			numer_budynku
		);
	END;
	$$ LANGUAGE plpgsql;


/*
funkcja rejestruje wizytę w danym oddziale
*/
CREATE OR REPLACE FUNCTION dodaj_wizyte(
        oddzial_id VARCHAR,
        rodzaj_wizyty VARCHAR,
        pesel_pacjenta VARCHAR(11),
        raport VARCHAR,
        wizyta_prywatna BOOLEAN,
        lekarz_id INTEGER,
        data_wizyty DATE
	) 
	RETURNS VOID AS 
	$$

	BEGIN
		INSERT INTO projekt.wizyta (
			oddzial_id, 
			rodzaj_wizyty, 
			pesel_pacjenta, 
			raport, 
			wizyta_prywatna, 
			lekarz_id, 
			data_wizyty
		) VALUES(
			oddzial_id, 
			rodzaj_wizyty, 
			pesel_pacjenta, 
			raport, 
			wizyta_prywatna, 
			lekarz_id, 
			data_wizyty
		);
	END;
	$$ LANGUAGE plpgsql;


/*
funckja dodaje kolejny rodzaj wizyty i ustala punkty wraz z wyceną za wizytę
*/
CREATE OR REPLACE FUNCTION dodaj_punkty(
        rodzaj_wizyty VARCHAR,
        liczba_punktow INTEGER,
        oplata_jesli_prywatna REAL
	) 
	RETURNS VOID AS 
	$$

	BEGIN
		INSERT INTO projekt.punkty_i_wyceny VALUES(
			rodzaj_wizyty,
			liczba_punktow,
			oplata_jesli_prywatna
		);
	END;
	$$ LANGUAGE plpgsql;

COMMIT;
