/*
tworzenie wszystkich tabel
*/

BEGIN;

CREATE SCHEMA projekt;

CREATE TABLE projekt.stawki_pracownicze (
                nazwa_stanowiska VARCHAR NOT NULL,
                stawka_refundowane INTEGER NOT NULL,
                stawka_komercyjne INTEGER,
                CONSTRAINT stawki_pracownicze_pk PRIMARY KEY (nazwa_stanowiska)
);


CREATE TABLE projekt.punkty_i_wyceny (
                rodzaj_wizyty VARCHAR NOT NULL,
                liczba_punktow INTEGER NOT NULL,
                oplata_jesli_prywatna REAL NOT NULL,
                CONSTRAINT punkty_i_wyceny_pk PRIMARY KEY (rodzaj_wizyty)
);


CREATE TABLE projekt.pacjent (
                pesel_pacjenta VARCHAR(11) NOT NULL,
                imie VARCHAR NOT NULL,
                nazwisko VARCHAR NOT NULL,
                telefon INTEGER NOT NULL,
                data_urodzenia DATE NOT NULL,
                miejsce_urodzenia VARCHAR NOT NULL,
                ubezpieczenie_emeryt_rencista BOOLEAN NOT NULL,
                ubezpieczenie_zatrudniony BOOLEAN NOT NULL,
                ubezpieczenie_informacja VARCHAR,
                CONSTRAINT pacjent_pk PRIMARY KEY (pesel_pacjenta)
);


CREATE TABLE projekt.osoba_upowazniona (
                telefon INTEGER NOT NULL,
                imie VARCHAR NOT NULL,
                nazwisko VARCHAR NOT NULL,
                pesel_pacjenta VARCHAR(11) NOT NULL,
                CONSTRAINT osoba_upowazniona_pk PRIMARY KEY (telefon)
);


CREATE TABLE projekt.adres_zamieszkania (
                pesel_pacjenta VARCHAR(11) NOT NULL,
                kod_pocztowy VARCHAR(6) NOT NULL,
                miejscowosc VARCHAR NOT NULL,
                ulica VARCHAR NOT NULL,
                numer_domu VARCHAR NOT NULL,
                CONSTRAINT adres_zamieszkania_pk PRIMARY KEY (pesel_pacjenta)
);


CREATE TABLE projekt.firma (
                nazwa_firmy VARCHAR NOT NULL,
                CONSTRAINT firma_pk PRIMARY KEY (nazwa_firmy)
);


CREATE TABLE projekt.oddzial (
                oddzial_id VARCHAR NOT NULL,
                nazwa_firmy VARCHAR NOT NULL,
                stawka_per_punkt_NFZ REAL NOT NULL,
                CONSTRAINT oddzial_pk PRIMARY KEY (oddzial_id)
);


CREATE TABLE projekt.pracownik (
                pracownik_id SERIAL NOT NULL,
                nazwa_stanowiska VARCHAR NOT NULL,
                data_podpisania_umowy DATE NOT NULL,
                data_rozwiazania_umowy DATE,
                imie VARCHAR NOT NULL,
                nazwisko VARCHAR NOT NULL,
                oddzial_id VARCHAR NOT NULL,
                CONSTRAINT pracownik_pk PRIMARY KEY (pracownik_id)
);


CREATE TABLE projekt.limit_punktowy (
                data_poczatek_rozliczenia DATE NOT NULL,
                oddzial_id VARCHAR NOT NULL,
                liczba_punktow INTEGER NOT NULL,
                data_koniec_rozliczenia DATE NOT NULL,
                CONSTRAINT limit_punktowy_pk PRIMARY KEY (data_poczatek_rozliczenia, oddzial_id)
);


CREATE TABLE projekt.lokalizacja_oddzialu (
                oddzial_id VARCHAR NOT NULL,
                kod_pocztowy VARCHAR(6) NOT NULL,
                miejscowosc VARCHAR NOT NULL,
                ulica VARCHAR NOT NULL,
                numer_lokalu VARCHAR NOT NULL,
                CONSTRAINT lokalizacja_oddzialu_pk PRIMARY KEY (oddzial_id)
);


CREATE TABLE projekt.sprzet_medyczny (
                numer_seryjny VARCHAR NOT NULL,
                nazwa VARCHAR NOT NULL,
                rok_produkcji INTEGER NOT NULL,
                oddzial_id VARCHAR NOT NULL,
                CONSTRAINT sprzet_medyczny_pk PRIMARY KEY (numer_seryjny)
);


CREATE TABLE projekt.paszport_techniczny (
                przeglad_id SERIAL NOT NULL,
                numer_seryjny VARCHAR NOT NULL,
                data_przegladu DATE NOT NULL,
                data_waznosci DATE NOT NULL,
                uwagi VARCHAR,
                CONSTRAINT paszport_techniczny_pk PRIMARY KEY (przeglad_id)
);


CREATE TABLE projekt.wizyta (
                wizyta_id SERIAL NOT NULL,
                oddzial_id VARCHAR NOT NULL,
                rodzaj_wizyty VARCHAR NOT NULL,
                pesel_pacjenta VARCHAR(11) NOT NULL,
                raport VARCHAR NOT NULL,
                wizyta_prywatna BOOLEAN NOT NULL,
                lekarz_id SERIAL NOT NULL,
                data_wizyty DATE NOT NULL,
                CONSTRAINT wizyta_pk PRIMARY KEY (wizyta_id, oddzial_id)
);


ALTER TABLE projekt.pracownik ADD CONSTRAINT stawki_pracownicze_pracownik_fk
FOREIGN KEY (nazwa_stanowiska)
REFERENCES projekt.stawki_pracownicze (nazwa_stanowiska)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.wizyta ADD CONSTRAINT punkty_NFZ_wizyta_komercyjna_fk
FOREIGN KEY (rodzaj_wizyty)
REFERENCES projekt.punkty_i_wyceny (rodzaj_wizyty)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.wizyta ADD CONSTRAINT pacjent_wizyty_fk
FOREIGN KEY (pesel_pacjenta)
REFERENCES projekt.pacjent (pesel_pacjenta)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.adres_zamieszkania ADD CONSTRAINT pacjent_adres_zamieszkania_fk
FOREIGN KEY (pesel_pacjenta)
REFERENCES projekt.pacjent (pesel_pacjenta)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.osoba_upowazniona ADD CONSTRAINT pacjent_osoba_upowazniona_fk
FOREIGN KEY (pesel_pacjenta)
REFERENCES projekt.pacjent (pesel_pacjenta)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.oddzial ADD CONSTRAINT firma_oddzial_fk
FOREIGN KEY (nazwa_firmy)
REFERENCES projekt.firma (nazwa_firmy)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.wizyta ADD CONSTRAINT oddzial_wizyty_fk
FOREIGN KEY (oddzial_id)
REFERENCES projekt.oddzial (oddzial_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.sprzet_medyczny ADD CONSTRAINT oddzial_sprzet_medyczny_fk
FOREIGN KEY (oddzial_id)
REFERENCES projekt.oddzial (oddzial_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.lokalizacja_oddzialu ADD CONSTRAINT oddzial_lokalizacja_oddzialu_fk
FOREIGN KEY (oddzial_id)
REFERENCES projekt.oddzial (oddzial_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.limit_punktowy ADD CONSTRAINT oddzial_limit_punktowy_fk
FOREIGN KEY (oddzial_id)
REFERENCES projekt.oddzial (oddzial_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.pracownik ADD CONSTRAINT oddzial_pracownik_fk
FOREIGN KEY (oddzial_id)
REFERENCES projekt.oddzial (oddzial_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.wizyta ADD CONSTRAINT pracownik_wizyta_komercyjna_fk
FOREIGN KEY (lekarz_id)
REFERENCES projekt.pracownik (pracownik_id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE projekt.paszport_techniczny ADD CONSTRAINT sprzet_medyczny_paszport_techniczny_fk
FOREIGN KEY (numer_seryjny)
REFERENCES projekt.sprzet_medyczny (numer_seryjny)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

COMMIT;
