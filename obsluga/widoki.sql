/*
przykładowe widoki
*/
BEGIN;

CREATE VIEW wszyscy_pacjenci AS 
	SELECT nazwisko, imie, pesel_pacjenta 
	FROM projekt.pacjent;

CREATE VIEW wszystkie_oddzialy AS 
	SELECT * 
	FROM projekt.lokalizacja_oddzialu;

CREATE VIEW zarobki_pracownikow AS 
	SELECT 
		projekt.pracownik.nazwisko, 
		projekt.pracownik.imie, 
		projekt.stawki_pracownicze.stawka_refundowane,
		CASE 
			WHEN projekt.pracownik.nazwa_stanowiska='lekarz'
			THEN '%'
			ELSE '[PLN]'
		END as rodzaj_stawki_refundowane,
		projekt.stawki_pracownicze.stawka_komercyjne,
		CASE 
			WHEN projekt.pracownik.nazwa_stanowiska='lekarz'
			THEN '%'
			ELSE '<brak>'
		END as rodzaj_stawki_komercyjne
	from projekt.stawki_pracownicze 
	join projekt.pracownik 
	on projekt.pracownik.nazwa_stanowiska=projekt.stawki_pracownicze.nazwa_stanowiska;

CREATE VIEW punkty AS
	SELECT * FROM projekt.punkty_i_wyceny;
	
COMMIT;

