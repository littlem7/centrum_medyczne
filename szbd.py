#!/usr/bin/python
# -*- coding: UTF-8 -*-

import psycopg2
import sys
import subprocess
from flask import Flask
from flask import *
from functools import wraps

reload(sys)
sys.setdefaultencoding('utf-8')


app = Flask(__name__)
app.config.update(SECRET_KEY='dummy_key')


# ****************************************************************************
# autentykacja
# ****************************************************************************
possible_users = {
    'admin': 'admin',
    'pracownik': 'pracownik'
}


@app.route("/")
def strona_glowna():
    if 'username' in session:
        return render_template('glowna.html')
    else:
        return redirect(url_for('login'))


@app.route("/login", methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        print request.form['password']
        print request.form['username']
        if request.form['password'] == possible_users.get(request.form['username']):
            session['username'] = request.form['username']
            session['logged_in'] = True
            return redirect(url_for('strona_glowna'))
        else:
            error = 'Invalid password or username'
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('username', None)
    session.pop('logged_in', None)
    return redirect(url_for('login'))


# ****************************************************************************
# obsluga bazy danych
# ****************************************************************************
def connect_to_database():
    try:
        conn = psycopg2.connect(database='u4bogusz', user='u4bogusz', password='4bogusz') 
        return conn
    except:
        print "I am unable to connect to the database"


def get_cursor():
    return connect_to_database().cursor()


def execute_sql_query(sql_query, title_of_query):
    cur = get_cursor()
    cur.execute(sql_query)
    names_of_columns = []
    columns = cur.description
    rows = cur.fetchall()
    for entity in columns:
        names_of_columns.append(entity.name.upper().replace('_', ' '))
    cur.close()
    return render_template(
        'tabela_z_powrotem.html', 
        entries=rows, 
        title=title_of_query, 
        titles=names_of_columns
    )

def execute_sql_query_with_params(sql_query, title_of_query, parms):
    cur = get_cursor()
    cur.execute(sql_query, parms)
    names_of_columns = []
    columns = cur.description
    rows = cur.fetchall()
    for entity in columns:
        names_of_columns.append(entity.name.upper().replace('_', ' '))
    cur.close()
    return render_template(
        'tabela_z_powrotem.html', 
        entries=rows, 
        title=title_of_query, 
        titles=names_of_columns
    )

# ****************************************************************************
# static css
# ****************************************************************************
@app.route("/css/<path:path>")
def send_css(path):
    return send_from_directory('templates', path)

# ****************************************************************************
# WIDOKI
# ****************************************************************************
@app.route("/zarobki")
def get_wszystkie_zarobki():
    if 'username' in session:
        return execute_sql_query('SELECT * from zarobki_pracownikow', 'Zarobki pracowników firmy')
    else:
        return redirect(url_for('login'))

@app.route("/pacjenci")
def get_wszyscy_pacjenci():
    if 'username' in session:
        return execute_sql_query('SELECT * from wszyscy_pacjenci', 'Wszyscy pacjenci firmy')
    else:
        return redirect(url_for('login'))

@app.route("/oddzialy")
def get_wszystkie_oddzialy():
    if 'username' in session:
        return execute_sql_query('SELECT * from wszystkie_oddzialy', 'Wszystkie oddziały firmy')
    else:
        return redirect(url_for('login'))

@app.route("/punkty")
def get_wszystkie_punkty():
    if 'username' in session:
        return execute_sql_query('SELECT * from punkty', 'Punkty za wizyty w firmie')
    else:
        return redirect(url_for('login'))


# ****************************************************************************
# SELECTY
# ****************************************************************************
@app.route("/licz_punkty_lekarza", methods=['GET', 'POST'])
def get_licz_punkty_lekarza():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('licz_punkty_lekarza.html')
        else:
            lekarz_id = request.form.get('lekarz_id')
            data_pocz = request.form.get('data_pocz')
            data_koniec = request.form.get('data_koniec')
            prywatne = request.form.get('prywatne')
            if prywatne == 'on':
                prywatne=True
            else:
                prywatne=False
            if not lekarz_id or not data_pocz or not data_koniec:
                return render_template('licz_punkty_lekarza.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from licz_punkty_lekarza(%s, %s, %s, %s)', 
                    'Punkty lekarza w danym okresie', [
                            lekarz_id,
                            data_pocz,
                            data_koniec,
                            prywatne
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('licz_punkty_lekarza.html', error='Baza zgłasza błąd w zapytaniu %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/stawka_pracownika", methods=['GET', 'POST'])
def get_stawka_pracownika():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('stawka_pracownika.html')
        else:
            pracownik_id = request.form.get('pracownik_id')
            if not pracownik_id:
                return render_template('stawka_pracownika.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from stawka_pracownika(%s)', 
                    'Stawka pracownika o danym id', [
                            pracownik_id
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('stawka_pracownika.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/wyplata_lekarza_za_okres", methods=['GET', 'POST'])
def get_wyplata_lekarza_za_okres():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('wyplata_lekarza_za_okres.html')
        else:
            lekarz_id = request.form.get('lekarz_id')
            data_pocz = request.form.get('data_pocz')
            data_koniec = request.form.get('data_koniec')
            if not lekarz_id or not data_koniec or not data_pocz:
                return render_template('wyplata_lekarza_za_okres.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from wyplata_lekarza_za_okres(%s, %s, %s)', 
                    'Stawka lekarza o danym id', [
                            lekarz_id,
                            data_pocz,
                            data_koniec
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('wyplata_lekarza_za_okres.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/adres_pacjenta", methods=['GET', 'POST'])
def get_adres_pacjenta():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('adres_pacjenta.html')
        else:
            pacjent_id = request.form.get('pacjent_id')
            if not pacjent_id:
                return render_template('adres_pacjenta.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from adres_pacjenta(%s)', 
                    'Adres pacjenta o danym id', [
                            pacjent_id
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('adres_pacjenta.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/wyplata_pracownika_za_okres", methods=['GET', 'POST'])
def get_wyplata_pracownika_za_okres():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('wyplata_pracownika_za_okres.html')
        else:
            pracownik_id = request.form.get('pracownik_id')
            if not pracownik_id:
                return render_template('wyplata_pracownika_za_okres.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from wyplata_pracownika_za_okres(%s)', 
                    'Wypłata pracownika za dany okres', [
                            pracownik_id
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('wyplata_pracownika_za_okres.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/lokalizacja_oddzialu", methods=['GET', 'POST'])
def get_lokalizacja_oddzialu():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('lokalizacja_oddzialu.html')
        else:
            oddzial_id = request.form.get('oddzial_id')
            if not oddzial_id:
                return render_template('lokalizacja_oddzialu.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from lokalizacja_oddzialu(%s)', 
                    'Adres oddziału o danym id', [
                            oddzial_id
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('lokalizacja_oddzialu.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/osoba_upowazniona_pacjenta", methods=['GET', 'POST'])
def get_osoba_upowazniona_pacjenta():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('osoba_upowazniona_pacjenta.html')
        else:
            pacjent_id = request.form.get('pacjent_id')
            if not pacjent_id:
                return render_template('osoba_upowazniona_pacjenta.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from osoba_upowazniona_pacjenta(%s)', 
                    'Informacje o osobach upoważnionych przez pacjenta o danym peselu', [
                            pacjent_id
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('osoba_upowazniona_pacjenta.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/sprzet_oddzialu", methods=['GET', 'POST'])
def get_sprzet_oddzialu():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('sprzet_oddzialu.html')
        else:
            oddzial_id = request.form.get('oddzial_id')
            if not oddzial_id:
                return render_template('sprzet_oddzialu.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from sprzet_oddzialu(%s)', 
                    'Informacje o sprzęcie medycznym dla oddziału o danym id', [
                            oddzial_id
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('sprzet_oddzialu.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/limity_oddzialu", methods=['GET', 'POST'])
def get_limity_oddzialu():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('limity_oddzialu.html')
        else:
            oddzial_id = request.form.get('oddzial_id')
            if not oddzial_id:
                return render_template('limity_oddzialu.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from limity_oddzialu(%s)', 
                    'Informacje o limitach punktowych dla oddziału o danym id', [
                            oddzial_id
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('limity_oddzialu.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/punkty_oddzialu_za_okres", methods=['GET', 'POST'])
def get_punkty_oddzialu_za_okres():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('punkty_oddzialu_za_okres.html')
        else:
            oddzial_id = request.form.get('oddzial_id')
            data_pocz = request.form.get('data_pocz')
            data_koniec = request.form.get('data_koniec')
            if not oddzial_id or not data_koniec or not data_pocz:
                return render_template('punkty_oddzialu_za_okres.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from punkty_oddzialu_za_okres(%s, %s, %s)', 
                    'Punkty oddziału za dany okres rozliczeniowy', [
                            oddzial_id,
                            data_pocz,
                            data_koniec
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('punkty_oddzialu_za_okres.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/pracownicy_oddzialu", methods=['GET', 'POST'])
def get_pracownicy_oddzialu():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('pracownicy_oddzialu.html')
        else:
            oddzial_id = request.form.get('oddzial_id')
            czy_lekarz = request.form.get('czy_lekarz')
            if czy_lekarz == 'on':
                czy_lekarz=True
            else:
                czy_lekarz=False
            if not oddzial_id:
                return render_template('pracownicy_oddzialu.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from pracownicy_oddzialu(%s, %s)', 
                    'Pracownicy danego odddziału', [
                            oddzial_id,
                            czy_lekarz
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('pracownicy_oddzialu.html', error='Baza zgłasza błąd w zapytaniu %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/zarobek_oddzialu_za_okres", methods=['GET', 'POST'])
def get_zarobek_oddzialu_za_okres():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('zarobek_oddzialu_za_okres.html')
        else:
            oddzial_id = request.form.get('oddzial_id')
            data_pocz = request.form.get('data_pocz')
            data_koniec = request.form.get('data_koniec')
            if not oddzial_id or not data_koniec or not data_pocz:
                return render_template('zarobek_oddzialu_za_okres.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_query_with_params(
                    'SELECT * from zarobek_oddzialu_za_okres(%s, %s, %s)', 
                    'Zarobek oddziału w danym okresie rozliczeniowym', [
                            oddzial_id,
                            data_pocz,
                            data_koniec
                        ]
                    )
                    return input_execute
                except Exception as e:
                    return render_template('zarobek_oddzialu_za_okres.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

def execute_sql_query_with_limits(sql_query, title_of_query, params, exact_function):
    cur = get_cursor()
    cur.execute(sql_query, params)
    names_of_columns = []
    columns = cur.description
    rows = cur.fetchall()
    for entity in columns:
        names_of_columns.append(entity.name.upper().replace('_', ' '))
    cur.close()
    return render_template(
        'karty_limitowane' + exact_function + '.html', 
        entries=rows, 
        title=title_of_query, 
        titles=names_of_columns,
    )

pacjenci_limit_curr = 1

@app.route("/pacjenci_limit", methods=['GET', 'POST'])
def get_pacjenci_limit():
    global pacjenci_limit_curr
    wielkosc_limtu=1
    if 'username' in session:
        if request.method == 'GET':
            pacjenci_limit_curr = 1
            try:
                input_execute = execute_sql_query_with_limits(
                    'SELECT * from pacjenci_limit(%s, %s)', 
                    'Wszyscy pacjenci', [
                        wielkosc_limtu,
                        pacjenci_limit_curr
                    ],
                    '_pacjenci_limit'
                )
                return input_execute
            except Exception as e:
                return render_template('glowna.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
        else:
            prawy = request.form.get('P')
            lewy = request.form.get('L')
            if lewy == None:
                pacjenci_limit_curr += 1
            else:
                pacjenci_limit_curr -= 1
            try:
                input_execute = execute_sql_query_with_limits(
                'SELECT * from pacjenci_limit(%s, %s)', 
                'Wszyscy pacjenci', [
                        wielkosc_limtu,
                        pacjenci_limit_curr
                    ],
                    '_pacjenci_limit'
                )
                return input_execute
            except Exception as e:
                return render_template('glowna.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

wizyty_limit_curr = 1

@app.route("/wizyty_limit", methods=['GET', 'POST'])
def get_wizyty_limit():
    global wizyty_limit_curr
    wielkosc_limtu=2
    if 'username' in session:
        if request.method == 'GET':
            wizyty_limit_curr = 1
            try:
                input_execute = execute_sql_query_with_limits(
                    'SELECT * from wizyty_limit(%s, %s)', 
                    'Wszystkie wizyty', [
                        wielkosc_limtu,
                        wizyty_limit_curr
                    ],
                    '_wizyty_limit'
                )
                return input_execute
            except Exception as e:
                return render_template('glowna.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
        else:
            prawy = request.form.get('P')
            lewy = request.form.get('L')
            if lewy == None:
                wizyty_limit_curr += 1
            else:
                wizyty_limit_curr -= 1
            try:
                input_execute = execute_sql_query_with_limits(
                'SELECT * from wizyty_limit(%s, %s)', 
                'Wszystkie wizyty', [
                        wielkosc_limtu,
                        wizyty_limit_curr
                    ],
                    '_wizyty_limit'
                )
                return input_execute
            except Exception as e:
                return render_template('glowna.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))


# ****************************************************************************
# INSERTY z funkcji
# ****************************************************************************
@app.route("/dodaj_oddzial", methods=['GET', 'POST'])
def dodaj_oddzial():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_oddzial.html')
        else:
            id_oddzialu = request.form.get('id_oddzialu')
            stawka_za_punkt = request.form.get('stawka_za_punkt')
            kod_pocztowy = request.form.get('kod_pocztowy')
            miejscowosc = request.form.get('miejscowosc')
            nazwa_ulicy = request.form.get('nazwa_ulicy')
            numer_budynku = request.form.get('numer_budynku')
            nazwa_firmy = request.form.get('nazwa_firmy')
            if not id_oddzialu or not stawka_za_punkt or not kod_pocztowy or not miejscowosc or not nazwa_firmy or not nazwa_ulicy or not numer_budynku:
                return render_template('dodaj_oddzial.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_oddzial(%s, %s, %s, %s, %s, %s, %s)', 
                        [
                            id_oddzialu,
                            stawka_za_punkt,
                            kod_pocztowy,
                            miejscowosc,
                            nazwa_ulicy,
                            numer_budynku,
                            nazwa_firmy
                        ],
                        'Dodano oddział'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_oddzial.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

def execute_sql_insert_func(sql_query, parms, title_of_query):
    conn = connect_to_database()
    cur = conn.cursor()
    cur.execute(sql_query, parms)
    conn.commit()
    #cur.close()
    return render_template(
        'tabela_z_powrotem.html', 
        entries={}, 
        title=title_of_query, 
        titles=[]
    )

@app.route("/dodaj_sprzet_medyczny", methods=['GET', 'POST'])
def dodaj_sprzet_medyczny():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_sprzet_medyczny.html')
        else:
            numer_seryjny = request.form.get('numer_seryjny')
            nazwa = request.form.get('nazwa')
            rok_produkcji = request.form.get('rok_produkcji')
            oddzial_id = request.form.get('oddzial_id')
            data_przegladu = request.form.get('data_przegladu')
            data_waznosci = request.form.get('data_waznosci')
            uwagi = request.form.get('uwagi')
            if not uwagi:
                uwagi = ''

            if not numer_seryjny or not nazwa or not rok_produkcji or not oddzial_id or not data_przegladu or not data_waznosci:
                return render_template('dodaj_sprzet_medyczny.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_sprzet_medyczny(%s, %s, %s, %s, %s, %s, %s)', 
                        [
                            numer_seryjny,
                            nazwa,
                            rok_produkcji,
                            oddzial_id,
                            data_przegladu,
                            data_waznosci,
                            uwagi
                        ],
                        'Dodano sprzęt medyczny'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_sprzet_medyczny.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/dodaj_przeglad_sprzetu_do_paszportu", methods=['GET', 'POST'])
def dodaj_przeglad_sprzetu_do_paszportu():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_przeglad_sprzetu_do_paszportu.html')
        else:
            numer_seryjny = request.form.get('numer_seryjny')
            data_przegladu = request.form.get('data_przegladu')
            data_waznosci = request.form.get('data_waznosci')
            uwagi = request.form.get('uwagi')
            if not uwagi:
                uwagi = ''

            if not numer_seryjny or not data_przegladu or not data_waznosci:
                return render_template('dodaj_przeglad_sprzetu_do_paszportu.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_przeglad_sprzetu_do_paszportu(%s, %s, %s, %s)', 
                        [
                            numer_seryjny,
                            data_przegladu,
                            data_waznosci,
                            uwagi
                        ],
                        'Dodano wpis do paszportu sprzętu medycznego'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_przeglad_sprzetu_do_paszportu.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/dodaj_limit_punktowy_dla_oddzialu", methods=['GET', 'POST'])
def dodaj_limit_punktowy_dla_oddzialu():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_limit_punktowy_dla_oddzialu.html')
        else:
            data_poczatek_rozliczenia = request.form.get('data_poczatek_rozliczenia')
            oddzial_id = request.form.get('oddzial_id')
            liczba_punktow = request.form.get('liczba_punktow')
            data_koniec_rozliczenia = request.form.get('data_koniec_rozliczenia')

            if not data_poczatek_rozliczenia or not oddzial_id or not liczba_punktow or not data_koniec_rozliczenia:
                return render_template('dodaj_limit_punktowy_dla_oddzialu.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_limit_punktowy_dla_oddzialu(%s, %s, %s, %s)', 
                        [
                            data_poczatek_rozliczenia,
                            oddzial_id,
                            liczba_punktow,
                            data_koniec_rozliczenia
                        ],
                        'Dodano limit punktowy dla oddziału'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_limit_punktowy_dla_oddzialu.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/dodaj_nowa_posade", methods=['GET', 'POST'])
def dodaj_nowa_posade():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_nowa_posade.html')
        else:
            nazwa_stanowiska = request.form.get('nazwa_stanowiska')
            stawka_pracownicza = request.form.get('stawka_pracownicza')

            if not nazwa_stanowiska or not stawka_pracownicza:
                return render_template('dodaj_nowa_posade.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_nowa_posade(%s, %s)', 
                        [
                            nazwa_stanowiska,
                            stawka_pracownicza
                        ],
                        'Dodano nową posadę'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_nowa_posade.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/dodaj_pracownika", methods=['GET', 'POST'])
def dodaj_pracownika():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_pracownika.html')
        else:
            nazwa_stanowiska = request.form.get('nazwa_stanowiska')
            data_podpisania_umowy = request.form.get('data_podpisania_umowy')
            data_rozwiazania_umowy = request.form.get('data_rozwiazania_umowy')
            imie = request.form.get('imie')
            nazwisko = request.form.get('nazwisko')
            oddzial_id = request.form.get('oddzial_id')

            if not nazwa_stanowiska or not data_podpisania_umowy or not data_rozwiazania_umowy or not imie or not nazwisko or not oddzial_id:
                return render_template('dodaj_pracownika.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_pracownika(%s, %s, %s, %s, %s, %s)', 
                        [
                            nazwa_stanowiska,
                            data_podpisania_umowy,
                            data_rozwiazania_umowy,
                            imie,
                            nazwisko,
                            oddzial_id
                        ],
                        'Dodano pracownika'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_pracownika.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/dodaj_punkty", methods=['GET', 'POST'])
def dodaj_punkty():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_punkty.html')
        else:
            rodzaj_wizyty = request.form.get('rodzaj_wizyty')
            liczba_punktow = request.form.get('liczba_punktow')
            oplata_jesli_prywatna = request.form.get('oplata_jesli_prywatna')

            if not rodzaj_wizyty or not liczba_punktow or not oplata_jesli_prywatna:
                return render_template('dodaj_punkty.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_punkty(%s, %s, %s)', 
                        [
                            rodzaj_wizyty,
                            liczba_punktow,
                            oplata_jesli_prywatna
                        ],
                        'Dodano punkty'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_punkty.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/dodaj_wizyte", methods=['GET', 'POST'])
def dodaj_wizyte():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_wizyte.html')
        else:
            oddzial_id = request.form.get('oddzial_id')
            rodzaj_wizyty = request.form.get('rodzaj_wizyty')
            pesel_pacjenta = request.form.get('pesel_pacjenta')
            raport = request.form.get('raport')
            wizyta_prywatna = request.form.get('wizyta_prywatna')
            if wizyta_prywatna == 'on':
                wizyta_prywatna=True
            else:
                wizyta_prywatna=False
            lekarz_id = request.form.get('lekarz_id')
            data_wizyty = request.form.get('data_wizyty')

            if not oddzial_id or not rodzaj_wizyty or not pesel_pacjenta or not raport or not lekarz_id or not data_wizyty:
                return render_template('dodaj_wizyte.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_wizyte(%s, %s, %s, %s, %s, %s, %s)', 
                        [
                            oddzial_id,
                            rodzaj_wizyty,
                            pesel_pacjenta,
                            raport,
                            wizyta_prywatna,
                            lekarz_id,
                            data_wizyty
                        ],
                        'Dodano wizytę'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_wizyte.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))

@app.route("/dodaj_pacjenta", methods=['GET', 'POST'])
def dodaj_pacjenta():
    if 'username' in session:
        if request.method == 'GET':
            return render_template('dodaj_pacjenta.html')
        else:
            pesel_pacjenta = request.form.get('pesel_pacjenta')
            imie = request.form.get('imie')
            nazwisko = request.form.get('nazwisko')
            telefon = request.form.get('telefon')
            data_urodzenia = request.form.get('data_urodzenia')
            miejsce_urodzenia = request.form.get('miejsce_urodzenia')
            ubezpieczenie_emeryt_rencista = request.form.get('ubezpieczenie_emeryt_rencista')
            if ubezpieczenie_emeryt_rencista == 'on':
                ubezpieczenie_emeryt_rencista=True
            else:
                ubezpieczenie_emeryt_rencista=False
            ubezpieczenie_zatrudniony = request.form.get('ubezpieczenie_zatrudniony')
            if ubezpieczenie_zatrudniony == 'on':
                ubezpieczenie_zatrudniony=True
            else:
                ubezpieczenie_zatrudniony=False
            ubezpieczenie_informacja = request.form.get('ubezpieczenie_informacja')
            kod_pocztowy = request.form.get('kod_pocztowy')
            miejscowosc_zamieszkania = request.form.get('miejscowosc_zamieszkania')
            ulica = request.form.get('ulica')
            numer_budynku = request.form.get('numer_budynku')
            telefon_osoby_upowaznionej = request.form.get('telefon_osoby_upowaznionej')
            imie_osoby_upowaznionej = request.form.get('imie_osoby_upowaznionej')
            nazwisko_osoby_upowaznionej = request.form.get('nazwisko_osoby_upowaznionej')

            if not pesel_pacjenta or not imie or not nazwisko or not telefon or not data_urodzenia or not miejsce_urodzenia or not kod_pocztowy or not miejscowosc_zamieszkania or not ulica or not numer_budynku or not telefon_osoby_upowaznionej or not imie_osoby_upowaznionej or not nazwisko_osoby_upowaznionej:
                return render_template('dodaj_pacjenta.html', error='Błędne wypełnienie pól')
            else:
                try:
                    input_execute = execute_sql_insert_func(
                        'SELECT * FROM dodaj_pacjenta(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', 
                        [
                            pesel_pacjenta,
                            imie,
                            nazwisko,
                            telefon,
                            data_urodzenia,
                            miejsce_urodzenia,
                            ubezpieczenie_emeryt_rencista,
                            ubezpieczenie_zatrudniony,
                            ubezpieczenie_informacja,
                            kod_pocztowy,
                            miejscowosc_zamieszkania,
                            ulica,
                            numer_budynku,
                            telefon_osoby_upowaznionej,
                            imie_osoby_upowaznionej,
                            nazwisko_osoby_upowaznionej
                        ],
                        'Dodano pacjenta'
                    )
                    return input_execute
                except Exception as e:
                    return render_template('dodaj_pacjenta.html', error='Baza zgłasza błąd w zapytaniu: %s' % e)
    else:
        return redirect(url_for('login'))


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5039)
